import requests

class VlcController:

        #curl -H 'Authorization: Basic OmxhamVhbm5l' -v http://127.0.0.1:8080/requests/status.xml?command=pl_next
    def playNext():
        requests.get('http://127.0.0.1:8080/requests/status.xml?command=pl_next', headers={'Authorization': 'Basic OmxhamVhbm5l'})
        
    def playPrev():
        requests.get('http://127.0.0.1:8080/requests/status.xml?command=pl_previous', headers={'Authorization': 'Basic OmxhamVhbm5l'})
        
    def pause():
        requests.get('http://127.0.0.1:8080/requests/status.xml?command=pl_pause', headers={'Authorization': 'Basic OmxhamVhbm5l'})

    def play():
        requests.get('http://127.0.0.1:8080/requests/status.xml?command=pl_play', headers={'Authorization': 'Basic OmxhamVhbm5l'})
