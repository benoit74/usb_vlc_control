

class BuzzButton:

    def __init__(self):
        self.light = 0
        self.pressed = 0

    def lightUp(self):
        self.light = 1

    def lightDown(self):
        self.light = 0