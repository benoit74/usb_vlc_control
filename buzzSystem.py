
import hid
import sys
from buzzDevice import BuzzDevice

class BuzzSystem:

    def __init__(self):

        # enumerate USB devices
        #print("Enumerating devices")
        #for d in hid.enumerate():
        #    keys = list(d.keys())
        #    keys.sort()
        #    for key in keys:
        #        print("%s : %s" % (key, d[key]))
        #    print()
        
        print("Finding Buzz devices")
        vendor_id = 0x054c
        product_id = 0x0002
        device_list = hid.enumerate(vendor_id, product_id)
        
        self.devices = []
        for device_dict in device_list:
            device = BuzzDevice(device_dict['path'])
            self.devices.append(device)
        
        nbDevices = len(self.devices)
        if (nbDevices == 0 or nbDevices > 2):
            print('Nombre de buzzers incorrect ({}). Veuillez connecter 4 à 8 buzzers.'.format(nbDevices * 4))
            sys.exit(-1)

        self.button1 = self.devices[0].button1
        self.button2 = self.devices[0].button2
        self.button3 = self.devices[0].button3
        self.button4 = self.devices[0].button4

        if (len(self.devices) >= 2):
            self.button5 = self.devices[1].button1
            self.button6 = self.devices[1].button2
            self.button7 = self.devices[1].button3
            self.button8 = self.devices[1].button4
        
        self.mainButton = self.button1
        self.mainButtonBlue = self.devices[0].button1Blue
        self.mainButtonOrange = self.devices[0].button1Orange
        
        if (len(self.devices) >= 2):
            self.regularButtons = [self.button2, self.button3, self.button4, self.button5, self.button6, self.button7, self.button8 ]
        else:
            self.regularButtons = [self.button2, self.button3, self.button4]
        
    def writeStatus(self):
        for device in self.devices:
            device.writeStatus()

    def readStatus(self):
        for device in self.devices:
            device.readStatus()

    def lightUp(self):
        for device in self.devices:
            device.lightUp()

    def lightDown(self):
        for device in self.devices:
            device.lightDown()