
import hid
from buzzButton import BuzzButton

class BuzzDevice:

    def __init__(self, path):
        self.device = hid.device()
        self.device.open_path(path)
        self.device.set_nonblocking(1)

        self.button1 = BuzzButton()
        self.button2 = BuzzButton()
        self.button3 = BuzzButton()
        self.button4 = BuzzButton()
        self.button1Blue = BuzzButton()
        self.button1Orange = BuzzButton()
        self.button1Green = BuzzButton()
        self.button1Yellow = BuzzButton()

        self.buttons = [self.button1, self.button2, self.button3, self.button4]
        #print("Manufacturer: %s" % self.device.get_manufacturer_string())
        #print("Serial No: %s" % self.device.get_serial_number_string())
        #print("Product: %s" % self.device.get_product_string())
    
    def writeStatus(self):
        self.device.write([0, 0, self.button1.light, self.button2.light, self.button3.light, self.button4.light])

    def readStatus(self):
        d = self.device.read(5)
        if d:
            binValue = '{:08b}{:08b}{:08b}'.format(d[2],d[3],d[4])
            self.button1.pressed = (binValue[7] == '1')
            self.button2.pressed = (binValue[2] == '1')
            self.button3.pressed = (binValue[13] == '1')
            self.button4.pressed = (binValue[8] == '1')
            self.button1Blue.pressed = (binValue[3] == '1')
            self.button1Orange.pressed = (binValue[4] == '1')
            self.button1Green.pressed = (binValue[5] == '1')
            self.button1Yellow.pressed = (binValue[6] == '1')

    def lightUp(self):
        for button in self.buttons:
            button.lightUp()

    def lightDown(self):
        for button in self.buttons:
            button.lightDown()

