# usb_vlc_control

Control VLC playback (i.e. stop it) from a USB device (currently Buzz).

Typically used for blind test games.

## Requirements for packaging on Windows

See for embedded installation:
https://stackoverflow.com/a/48906746

pip can then be used to install requests, hidapi, and other python packages needed

## Requirements for dev

- libusb (or not, to be checked, seems that hidapi is not using it indeed)
- Python 3
- requests, hidapi

### libusb

On Mac
```
brew install libusb
```

### Python

On Mac
```
brew install python
```

### requests, hidapi

```
pip install requests hidapi
```
