import time
import sys
import random

from buzzDevice import BuzzDevice
from buzzSystem import BuzzSystem
from vlcController import VlcController

# play buzz game

try:
    
    buzzSystem = BuzzSystem()

    print("Let's play!")
    buzzed = False
    blueButtonPressed = False
    orangeButtonPressed = False
    blinking = 0

    while True:
        if (not(buzzed)):
            if (blinking > 10):
              buzzSystem.button1.lightDown()
            else: 
              buzzSystem.button1.lightUp()
            blinking = blinking + 1
            if (blinking > 20):
                blinking = 0

        buzzSystem.readStatus()
            
        if (buzzed):
            if (buzzSystem.mainButton.pressed):
                buzzed = False
                buzzSystem.lightDown()
                VlcController.play()

            if (buzzSystem.mainButtonBlue.pressed):
                buzzed = False
                blueButtonPressed = True
                buzzSystem.lightDown()
                VlcController.playNext()
                
        else:

            if (buzzSystem.mainButton.pressed):
                VlcController.play()

            if (blueButtonPressed):
                if (not(buzzSystem.mainButtonBlue.pressed)):
                    blueButtonPressed = False
            else:
                if (buzzSystem.mainButtonBlue.pressed):
                    VlcController.playNext()
                    blueButtonPressed = True

            if (orangeButtonPressed):
                if (not(buzzSystem.mainButtonOrange.pressed)):
                    orangeButtonPressed = False
            else:
                if (buzzSystem.mainButtonOrange.pressed):
                    VlcController.playPrev()
                    orangeButtonPressed = True
                    
            pressedButtons = []
            for button in buzzSystem.regularButtons:
                if button.pressed:
                    pressedButtons.append(button)
            
            if (len(pressedButtons) >= 1):
                pressedButton = random.choice(pressedButtons)
                buzzed = True
                buzzSystem.mainButton.lightUp()
                pressedButton.lightUp()
                VlcController.pause()
        
        buzzSystem.writeStatus()
        time.sleep(0.05)

    buzzSystem.lightDown()

except IOError as ex:
    print(ex)
    print("Fatal error")

print("Done")